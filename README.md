# 2024 IDS721 Mini-Project 1
## Name: Ziyu Shi, NetID: zs148

**GitLab link:** `git@gitlab.com:JackShi123/duke-ids721-jackshi.git`

Here is my mini-project 1. Please follow the steps to launch the website.

Please `git clone git@gitlab.com:JackShi123/duke-ids721-jackshi.git`

Then enter `zola serve` in the terminal, the static website will be launched on the localhost.

In the following, I will show the screenshots of part of the running static site:

1. **Home page**
![home page](static/photos/home.png)

2. **About page (portfolio page)**
![about page](static/photos/about.png)
In this page, I introduce myself in detail.

3. **Education page**
![education page](static/photos/education.png)
In this page, I introduce my undergraudate university and graudate university.

4. **Skills page** 
![Skills page](static/photos/skills.png)
In this page, I introduce the programming skills I learned.

5. **Projects**
![Projects page](static/photos/projects.png)
In this page, I introduce two projects accomplished during Duke.