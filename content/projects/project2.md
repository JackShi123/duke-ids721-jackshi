+++
title = "Mini-UPS"

[extra]
image = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/United_Parcel_Service_logo_2014.svg/1718px-United_Parcel_Service_logo_2014.svg.png"
link = "https://www.example.com"
technologies = ["Python", "Socket", "Django", "Protobuf"]
+++
This platform enables customers to seamlessly track their packages. I implemented a robust intercommunication protocol utilizing Google Protocol Buffer and established a resilient back-end server for seamless interactions with other servers and the database. The front-end, developed with Django, offers users an intuitive interface for tracking and monitoring package status. I ensured data integrity in PostgreSQL through careful manipulation and rollback operations, and maintained message transmission reliability through a seq/ack mechanism and retransmission.