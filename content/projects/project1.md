+++
title = "HTTP-Caching-Proxy"

[extra]
image = "https://static-00.iconduck.com/assets.00/proxy-server-icon-2048x2048-uowyfh3a.png"
link = "https://www.example.com"
technologies = ["C++", "Socket", "HTTP", "Thread pool"]
+++
Introducing our HTTP Proxy Server with Cache, a robust solution crafted in C++ leveraging socket programming, pthreads, and TCP/IP protocols. This server adeptly handles GET, POST, and CONNECT HTTP requests, optimizing performance through a meticulously designed LRU cache. The cache minimizes unnecessary remote server access, enhancing response times. Employing a pre-thread method, our server efficiently manages various requests from distinct clients simultaneously. To streamline troubleshooting, a comprehensive log file has been implemented, ensuring easy identification of errors and facilitating effective debugging throughout the development process.