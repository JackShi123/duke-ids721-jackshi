+++
title = "About Me!"
template = "about.html"

[extra]
author = "Ziyu Shi"
image = "https://seeklogo.com/images/P/Pikachu-logo-D0AAA93F17-seeklogo.com.png"
+++

As a graduate student in the Electrical and Computer Engineering (ECE) department at Duke University, I bring a diverse academic background and a passion for technology and innovation. My journey in academia began at the University of Electronic Science and Technology of China, where I earned my undergraduate degree in Computer Science.

During my undergraduate studies, I developed a solid foundation in computer science principles, gaining expertise in areas such as algorithms, data structures, and software development. My coursework provided me with a strong theoretical background, and I engaged in hands-on projects that allowed me to apply these concepts to real-world challenges. This experience cultivated my problem-solving skills and ignited my interest in exploring advanced topics in the field.

Now, as a graduate student at Duke, I am eager to delve deeper into the realm of Electrical and Computer Engineering. The dynamic and interdisciplinary nature of the ECE department at Duke aligns perfectly with my academic interests. I am particularly drawn to research opportunities that involve cutting-edge technologies, such as artificial intelligence, machine learning, and embedded systems. I am excited to contribute to the ongoing advancements in these areas and to collaborate with esteemed faculty members and fellow researchers.

Beyond academics, I am actively involved in the university community, participating in seminars, workshops, and networking events. I believe in the importance of continuous learning and staying abreast of the latest developments in technology. With a commitment to academic excellence and a keen interest in pushing the boundaries of innovation, I am poised to make meaningful contributions to the field of Electrical and Computer Engineering during my graduate studies at Duke University.
